package kz.dars.admin.service;

import kz.dars.admin.domain.Place;

public interface PlaceService extends BaseService<Place> {

}
