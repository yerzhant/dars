package kz.dars.admin.domain.security;

import javax.persistence.Entity;
import kz.dars.admin.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class AppRole extends BaseEntity {

    @NotEmpty
    private String name;

    @NotEmpty
    private String code;
}
