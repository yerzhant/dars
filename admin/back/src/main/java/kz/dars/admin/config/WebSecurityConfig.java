package kz.dars.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                    .antMatchers("/api/**").authenticated();
    }

    @Bean
    public AuthService authService() {
        System.out.println("1111111111111111111111111");
        return new AuthService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        System.out.println("3333333333333333333333333");
        return new BCryptPasswordEncoder();
    }

    private static class AuthService implements UserDetailsService {

        @Override
        public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {

            System.out.println("++++++++++++++++++++++++++++++++++++++");
            UserDetails user = new User("user", "$2a$10$ikpZxpLUiuwEgUuy15OfF.ZF/RNz7J5BME95tNmmCJhUq.mRE5jWC",
                    true, true, true, true, AuthorityUtils.createAuthorityList("ROLE_admin"));
            return user;
        }

    }
}
