package kz.dars.admin.repository.security;

import kz.dars.admin.domain.security.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {

}
