package kz.dars.admin.service;

import java.util.List;
import kz.dars.admin.domain.BaseEntity;

public interface BaseService<T extends BaseEntity> {

    List<T> findAll();

    T findOne(Long id);

    T save(T entity);

    void delete(T entity);

    void delete(Long id);
}
