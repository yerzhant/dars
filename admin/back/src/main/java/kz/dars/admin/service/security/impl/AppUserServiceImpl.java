package kz.dars.admin.service.security.impl;

import kz.dars.admin.domain.security.AppUser;
import kz.dars.admin.repository.security.AppUserRepository;
import kz.dars.admin.service.impl.BaseServiceImpl;
import kz.dars.admin.service.impl.ServiceRepository;
import kz.dars.admin.service.security.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUserServiceImpl extends BaseServiceImpl<AppUser> implements AppUserService {

    @Autowired
    @ServiceRepository
    private AppUserRepository repository;
}
