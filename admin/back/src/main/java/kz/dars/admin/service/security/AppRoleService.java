package kz.dars.admin.service.security;

import kz.dars.admin.domain.security.AppRole;
import kz.dars.admin.service.BaseService;

public interface AppRoleService extends BaseService<AppRole> {

}
