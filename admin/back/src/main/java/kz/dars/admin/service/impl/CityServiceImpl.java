package kz.dars.admin.service.impl;

import kz.dars.admin.domain.City;
import kz.dars.admin.repository.CityRepository;
import kz.dars.admin.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl extends BaseServiceImpl<City> implements CityService {

    @Autowired
    @ServiceRepository
    private CityRepository repository;
}
