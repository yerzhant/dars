package kz.dars.admin.service;

import kz.dars.admin.domain.City;

public interface CityService extends BaseService<City> {

}
