package kz.dars.admin.service.impl;

import java.lang.reflect.Field;
import java.util.List;
import javax.annotation.PostConstruct;
import kz.dars.admin.domain.BaseEntity;
import kz.dars.admin.service.BaseService;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

    private JpaRepository<T, Long> repository;

    @PostConstruct
    public void init() throws IllegalArgumentException, IllegalAccessException {
        for (Field f : this.getClass().getDeclaredFields()) {
            if (f.isAnnotationPresent(ServiceRepository.class)) {
                f.setAccessible(true);
                repository = (JpaRepository<T, Long>) f.get(this);
                break;
            }
        }
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T findOne(Long id) {
        return repository.findOne(id);
    }

    @Override
    public T save(T entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(T entity) {
        repository.delete(entity);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }
}
