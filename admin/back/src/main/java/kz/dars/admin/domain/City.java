package kz.dars.admin.domain;

import javax.persistence.Entity;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class City extends BaseEntity {

    @NotEmpty
    private String name;
}
