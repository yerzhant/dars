package kz.dars.admin.service.security;

import kz.dars.admin.domain.security.AppUser;
import kz.dars.admin.service.BaseService;

public interface AppUserService extends BaseService<AppUser> {

}
