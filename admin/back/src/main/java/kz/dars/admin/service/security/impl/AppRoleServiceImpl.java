package kz.dars.admin.service.security.impl;

import kz.dars.admin.domain.security.AppRole;
import kz.dars.admin.repository.security.AppRoleRepository;
import kz.dars.admin.service.impl.BaseServiceImpl;
import kz.dars.admin.service.impl.ServiceRepository;
import kz.dars.admin.service.security.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppRoleServiceImpl extends BaseServiceImpl<AppRole> implements AppRoleService {

    @Autowired
    @ServiceRepository
    private AppRoleRepository repository;
}
