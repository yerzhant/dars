package kz.dars.admin.domain.security;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import kz.dars.admin.domain.BaseEntity;
import kz.dars.admin.domain.Place;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Value;

@Entity
@Data
public class AppUser extends BaseEntity {

    @NotEmpty
    private String name;

    @NotEmpty
    private String password;

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String firstName;

    private String surname;

    @NotNull
    @Min(0)
    @Value("app.security.login-tries")
    private Byte leftLoginTries;

    @NotNull
    private Boolean isLocked = false;

    @NotNull
    private Boolean isAdmin = false;

    @ManyToMany
    private Set<AppRole> roles;

    @ManyToMany
    private Set<Place> places;
}
