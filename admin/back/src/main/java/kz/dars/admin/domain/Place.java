package kz.dars.admin.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class Place extends BaseEntity {

    @NotNull
    @ManyToOne
    private City city;

    @NotEmpty
    private String name;

    @NotEmpty
    private String address;
}
