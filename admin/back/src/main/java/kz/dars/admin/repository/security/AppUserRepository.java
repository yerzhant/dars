package kz.dars.admin.repository.security;

import kz.dars.admin.domain.security.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
