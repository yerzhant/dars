package kz.dars.admin.service.impl;

import kz.dars.admin.domain.Place;
import kz.dars.admin.repository.PlaceRepository;
import kz.dars.admin.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;

public class PlaceServiceImpl extends BaseServiceImpl<Place> implements PlaceService {

    @Autowired
    @ServiceRepository
    private PlaceRepository repository;
}
