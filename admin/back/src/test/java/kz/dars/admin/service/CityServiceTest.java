package kz.dars.admin.service;

import kz.dars.admin.domain.City;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Transactional
public class CityServiceTest {

    @Autowired
    private CityService s;

    private City c;

    @Before
    public void setUp() {
        c = new City();
        c.setName("Test");
        s.save(c);
    }

    @Test
    public void testFindAll() {
        assertTrue(s.findAll().size() > 0);
    }
}
