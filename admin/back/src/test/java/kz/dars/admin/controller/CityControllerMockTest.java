package kz.dars.admin.controller;

import java.util.ArrayList;
import java.util.List;
import kz.dars.admin.domain.City;
import kz.dars.admin.service.CityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CityController.class)
public class CityControllerMockTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CityService s;

    @Test
    public void unauthorized() throws Exception {
        mvc.perform(get("/api/cities")).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void testFindAll() throws Exception {
        List<City> l = new ArrayList<>();
        l.add(new City());
        BDDMockito.given(s.findAll()).willReturn(l);

        mvc.perform(get("/api/cities").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
//                .andExpect(jsonPath("$", matcher));
    }

    @Test
    public void basicAuth() throws Exception {
        mvc
                .perform(get("/api/cities").with(httpBasic("user", "password")))
                //                .perform(get("/api/cities").header(HttpHeaders.AUTHORIZATION,
                //                        "Basic " + Base64Utils.encodeToString("user:password".getBytes())))
                .andExpect(status().isOk());
    }

    @Test
    public void auth() throws Exception {
        mvc
                .perform(get("/api/cities").with(user("user").password("password").roles("USER")))
                .andExpect(status().isOk());
    }
}
