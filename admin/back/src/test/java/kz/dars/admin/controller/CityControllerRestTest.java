package kz.dars.admin.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CityControllerRestTest {

    private final TestRestTemplate rest = new TestRestTemplate("user", "password", TestRestTemplate.HttpClientOption.SSL);

    @Test
    public void testFindAll() throws Exception {
        String s = rest.getForObject("http://localhost:8080/api/cities", String.class);
        System.out.println(s);
//        City[] a;
//        a = rest.getForObject("http://localhost:8080/api/cities", City[].class);
//        Arrays.stream(a).forEach(System.out::print);
    }
}
